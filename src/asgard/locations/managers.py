from django.db.models import Manager

class LocationManager(Manager):
	def get_query_set(self):
		return super(LocationManager, self).get_query_set().extra(
			select = {
				'address': 'SELECT location_points.address FROM location_points WHERE locations.point_id = location_points.id',
				'postal_code': 'SELECT location_points.postal_code FROM location_points WHERE locations.point_id = location_points.id',
				'latitude': 'SELECT location_points.latitude FROM location_points WHERE locations.point_id = location_points.id',
				'longitude': 'SELECT location_points.longitude FROM location_points WHERE locations.point_id = location_points.id',
				'city': 'SELECT location_cities.city FROM location_cities, location_points WHERE locations.point_id = location_points.id AND location_points.city_id = location_cities.id',
				'city_slug': 'SELECT location_cities.slug FROM location_cities, location_points WHERE locations.point_id = location_points.id AND location_points.city_id = location_cities.id',
				'province': 'SELECT location_cities.province FROM location_cities, location_points WHERE locations.point_id = location_points.id AND location_points.city_id = location_cities.id',
				'country': 'SELECT location_cities.country FROM location_cities, location_points WHERE locations.point_id = location_points.id AND location_points.city_id = location_cities.id'
			}
		)

class PointManager(Manager):
	def get_query_set(self):
		return super(PointManager, self).get_query_set().extra(
			select = {
				'city_name': 'SELECT location_cities.city FROM location_cities WHERE location_points.city_id = location_cities.id',
				'province': 'SELECT location_cities.province FROM location_cities WHERE location_points.city_id = location_cities.id',
				'country': 'SELECT location_cities.country FROM location_cities WHERE location_points.city_id = location_cities.id',
			}
		)