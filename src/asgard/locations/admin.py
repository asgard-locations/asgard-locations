from django.contrib import admin

from asgard.locations.models import *

admin.site.register(Location)
admin.site.register(Point)
admin.site.register(City)
admin.site.register(PlaceType)