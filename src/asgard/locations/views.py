from datetime import datetime

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from asgard.locations.models import *

def index(request, context={}, template_name='locations/index.html'):
	locations = Location.objects.all()
	
	context.update({
		'locations': locations,
	})
	
	return render_to_response(template_name, context, context_instance=RequestContext(request))

def city(request, city, context={}, template_name='locations/city.html'):
	try:
		city = City.objects.get(slug=city)
	except City.DoesNotExist:
		raise Http404
	
	context.update({
		'city': city
	})
	
	return render_to_response(template_name, context, context_instance=RequestContext(request))

def detail(request, location, city, context={}, template_name='locations/detail.html'):
	try:
		location = Location.objects.get(slug=location)
	except Location.DoesNotExist:
		raise Http404
	
	context.update({
		'location': location,
	})
	
	return render_to_response(template_name, context, context_instance=RequestContext(request))