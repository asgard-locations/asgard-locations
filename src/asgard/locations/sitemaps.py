from django.contrib.sitemaps import Sitemap

from asgard.locations.models import Location

class LocationsSitemap(Sitemap):
	changefreq = "never"
	priority = 1.0
	
	def items(self):
		return Location.objects.all()
	
	def lastmod(self, obj):
		return obj.date_modified