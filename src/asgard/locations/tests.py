from django.core.urlresolvers import reverse
from django.test import Client, TestCase

from asgard.locations.models import Location, City

client = Client()

class LocationsTestCase(TestCase):
	fixtures = ['locations',]
	
	def setUp(self):
		self.location_apple = City.objects.get(pk=1)
		self.location_google = City.objects.get(pk=2)
		self.city_toronto = Location.objects.get(pk=2)
		self.city_markham = Location.objects.get(pk=1)
	
	def testLocationsIndex(self):
		response = client.get(reverse('locations_index'))
		self.assertEquals(response.status_code, 200)
	
	def testLocationsCityDetail(self):
		response = client.get(self.city_toronto.get_absolute_url())
		self.assertEquals(response.status_code, 200)
	
	def testLocationsDetail(self):
		response = client.get(self.location_apple.get_absolute_url())
		self.assertEquals(response.status_code, 200)