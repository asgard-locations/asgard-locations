from haystack import site, indexes

from asgard.locations.models import Location

class LocationIndex(indexes.SearchIndex):
	text = indexes.CharField(document=True, use_template=True)

site.register(Location, LocationIndex)