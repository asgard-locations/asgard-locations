from django.conf.urls.defaults import *

urlpatterns = patterns('asgard.locations.views',
	url(r'^(?P<city>[-\w]+)/(?P<location>[-\w]+)/$',
		view = 'detail',
		name = 'locations_detail',
	),
	url(r'^(?P<city>[-\w]+)/$',
		view = 'city',
		name = 'locations_city',
	),
	url(r'^$',
		view = 'index',
		name = 'locations_index',
	)
)