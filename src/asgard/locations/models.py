from django.db import models
from django.db.models import permalink
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save, post_delete
from django.contrib.auth.models import User

from asgard.locations.managers import LocationManager, PointManager

class PlaceType(models.Model):
	""" Place types """
	title = models.CharField(_('title'), max_length=100, unique=True)
	slug = models.SlugField(_('slug'), unique=True)
	
	class Meta:
		verbose_name = _('place type')
		verbose_name_plural = _('place types')
		db_table = 'location_types'
	
	def __unicode__(self):
		return u"%s" % self.title

class City(models.Model):
	""" City model """
	city = models.CharField(_('city'), max_length=100)
	province = models.CharField(_('province'), max_length=100)
	country = models.CharField(_('country'), max_length=100)
	slug = models.SlugField(_('slug'), unique=True)
	
	class Meta:
		verbose_name = _('City')
		verbose_name_plural = _('Cities')
		db_table = 'location_cities'
		unique_together = (('city', 'province',),)
		ordering = ('province', 'city',)
	
	@permalink
	def get_absolute_url(self):
		return ('locations_city', None, {
			'city': self.slug,
		})
	
	def __unicode__(self):
		return u"%s, %s" % (self.city, self.province)

class Point(models.Model):
	""" Point model """
	latitude = models.FloatField(_('latitude'), blank=True, null=True)
	longitude = models.FloatField(_('longitude'), blank=True, null=True)
	address = models.CharField(_('address'), max_length=200, blank=True)
	city = models.ForeignKey(City)
	postal_code = models.CharField(_('postal code'), max_length=10, blank=True)
	woeid = models.IntegerField(_('WOEID'), blank=True, null=True)
	
	objects = PointManager()
	
	class Meta:
		verbose_name = _('Point')
		verbose_name_plural = _('Points')
		db_table = 'location_points'
	
	def __unicode__(self):
		return u"%s" % self.address
	
	def full_address(self):
		return u"%s %s %s %s %s" % (self.address, self.city.city, self.postal_code, self.city.province, self.city.country)

class Location(models.Model):
	title = models.CharField(_('Title'), max_length=200)
	slug = models.SlugField(_('Slug'), max_length=50)
	
	unit = models.CharField(_('unit'), blank=True, max_length=100, help_text='Suite or Apartment #')
	phone = models.CharField(_('phone'), blank=True, max_length=20)
	url = models.URLField(_('url'), blank=True, verify_exists=False)
	email = models.EmailField(_('email'), blank=True)
	description = models.TextField(_('description'), blank=True)
	
	point = models.ForeignKey(Point)
	
	objects = LocationManager()
	
	date_added = models.DateTimeField(_('date added'), auto_now_add=True)
	date_modified = models.DateTimeField(_('date modified'), auto_now=True)
	
	class Meta:
		verbose_name = _('Location')
		verbose_name_plural = _('Locations')
		db_table = 'locations'
	
	@permalink
	def get_absolute_url(self):
		return ('locations_detail', None, {
			'location': self.slug,
			'city': self.city_slug,
		})
	
	def __unicode__(self):
		return u"%s" % self.title
